<?php

include 'product_model.php';

	/**
	* Base class for DB connection and its operations
	*/
	class DBConnection
	{
		private $hostname = "localhost";
		private $username = "root";
		private $password = "";
		private $dbName = "scandiweb_db";
		private $tableName = "Products";
		private $connectivity;
		private $result = [];

		function __construct()
		{
			try{
				$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password,$this->dbName);
				$sqlTableExist = "SHOW TABLES LIKE '".$this->tableName."'";
				if (mysqli_query($this->connectivity, $sqlTableExist)) {
					$this->getData();
				}
				else {
					die("Table does not exist");
					$this->createTable();
				}
			} catch(Exception $e) {
				if (str_contains("$e", "Unknown database '".$this->dbName."'")) { 
					$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password);
				    // Create database
					$this->createDB();
				} else {
					die("connection failed: " . $e);
				}
			}
			$this->closeDBConnection();
		}

		private function closeDBConnection(){
			$this->connectivity->close();
		}

		private function createDB()
		{
			$sql = "CREATE DATABASE ".$this->dbName;
			if (mysqli_query($this->connectivity, $sql)) 
			{
				$this->connectivity->select_db($this->dbName);
				$this->createTable();
			} else {
				die("Error creating database: " . mysqli_error($this->connectivity));
			}
		}

		private function createTable()
		{
			$sqlTableCreate = "CREATE TABLE ".$this->tableName." (
			sku VARCHAR(10) NOT NULL PRIMARY KEY,
			name VARCHAR(255) NOT NULL,
			price INT(30) NOT NULL,
			type VARCHAR(50) NOT NULL,
			weight INT(50) UNSIGNED NULL,
			size INT(50) UNSIGNED NULL,
			height INT(50) UNSIGNED NULL,
			width INT(50) UNSIGNED NULL,
			length INT(50) UNSIGNED NULL
		)";
		if(!mysqli_query($this->connectivity, $sqlTableCreate)){
			die("Error creating table: " . mysqli_error($this->connectivity));
		}
	}

	public function setData()
	{
		$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password,$this->dbName);
		$sql = "INSERT INTO ".$this->tableName." (sku, name, price, type, size)
		VALUES ('".$this->getRandomString()."', 'John', '20', 'DVD', '50');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, weight)
		VALUES ('".$this->getRandomString()."', 'Marco', '20', 'BOOK', '60');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, width, height, length)
		VALUES ('".$this->getRandomString()."', 'Esabela', '20', 'Furniture', '10', '15', '25');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, size)
		VALUES ('".$this->getRandomString()."', 'John', '20', 'DVD', '50');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, weight)
		VALUES ('".$this->getRandomString()."', 'Marco', '20', 'BOOK', '60');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, width, height, length)
		VALUES ('".$this->getRandomString()."', 'Esabela', '20', 'Furniture', '10', '15', '25');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, size)
		VALUES ('".$this->getRandomString()."', 'John', '20', 'DVD', '50');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, weight)
		VALUES ('".$this->getRandomString()."', 'Marco', '20', 'BOOK', '60');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, width, height, length)
		VALUES ('".$this->getRandomString()."', 'Esabela', '20', 'Furniture', '10', '15', '25');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, size)
		VALUES ('".$this->getRandomString()."', 'John', '20', 'DVD', '50');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, weight)
		VALUES ('".$this->getRandomString()."', 'Marco', '20', 'BOOK', '60');";
		$sql .= "INSERT INTO ".$this->tableName." (sku, name, price, type, width, height, length)
		VALUES ('".$this->getRandomString()."', 'Esabela', '20', 'Furniture', '10', '15', '25')";	
		if(!mysqli_multi_query($this->connectivity, $sql)){
			die("Data couldn't be added!");
		}
		$this->closeDBConnection();
		header("Refresh:0");
	}

	function getRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';

		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, strlen($characters) - 1)];
		}

		return $string;
	}

	private function getData()
	{
		$sql = "SELECT * FROM ".$this->tableName;
		$query = mysqli_query($this->connectivity, $sql);
		if(!$query){
			die("couldn't retrieve data!");
		}
		while ($row = mysqli_fetch_array($query)) {
			$pm = new Product($row['sku'], $row['name'], $row['price'], $row['type'], $row['weight'], $row['size'], $row['width'], $row['height'], $row['length']);
			$this->result[] = $pm;
		}
	}

	private function getRowsNumber() {
		$sql = "SELECT * FROM " . $this->tableName;
		$stmt = mysqli_query($this->connectivity, $sql);
		if(!$stmt){
			die($this->tableName . " table couldn't be counted!");
		}
		$count = mysqli_num_rows($stmt);
		return $count;
	}

	public function getResult(){
		return $this->result;
	}

	public function addData($product){
		$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password,$this->dbName);
		$sql = "INSERT INTO ".$this->tableName." (sku, name, price, type, size, weight, width, height, length)
		VALUES ('".$product->getSku()."', '".$product->getName()."', '".$product->getPrice()."', '".$product->getType()."', '".$product->getSize()."', '".$product->getWeight()."', '".$product->getWidth()."', '".$product->getHeight()."', '".$product->getLength()."');";
		if(mysqli_query($this->connectivity, $sql)){
			$this->closeDBConnection();
			header("Refresh:0");
		}else{
			die("Product with name " . $product->getName() . " couldn't be Added");
		}
	}

	public function removeData($product){
		$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password,$this->dbName);
		$sql = "DELETE FROM ".$this->tableName." WHERE sku='".$product->getSku()."'";
		if(mysqli_query($this->connectivity, $sql)){
			$this->closeDBConnection();
			header("Refresh:0");
		}else{
			die("Product with name " . $product->getName() . " couldn't be Deleted");
		}		
	}

	public function removeAllData(){
		$this->connectivity = mysqli_connect($this->hostname,$this->username,$this->password,$this->dbName);
		if(mysqli_query($this->connectivity, "TRUNCATE TABLE " . $this->tableName)){
			$this->closeDBConnection();
			header("Refresh:0");
		}else{
			die($this->tableName . " table couldn't be tancated!");
		}
	}
}

$db = new DBConnection();
?>