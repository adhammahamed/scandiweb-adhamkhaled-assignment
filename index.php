<?php
require 'connection_db.php';

if(isset($_POST['TruncateProducts']) && count($db->getResult()) > 0) {
	$db->removeAllData();
}

if(isset($_POST['AddDummyData'])) {
	$db->setData();
}

for ($i=0; $i < count($db->getResult()); $i++) {
	if(isset($_POST['DELETE'.$i])) {
		$db->removeData($db->getResult()[$i]);
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.delete-checkbox{
			margin-top: 15px;
			margin-left: 15px;
			width: 15px;
			height: 15px;
		}
	</style>
</head>
<body style="margin: 0;">
	<header>
		<table style="width: 100%;">
			<tr>
				<th style="width: 80%;text-align: left;padding-left: 100px;">
					<h1>Product List</h1>
				</th>
				<th style="align-content: center;padding-right: 10px;">
					<a href="product_add.php">
						<button style="padding-right: 30px;padding-left: 30px;padding-top: 10px;padding-bottom: 10px;">ADD</button>
					</a>
				</th>
				<th style="align-content: center;padding-right: 10px;">
					<form method="post">
						<input type="submit" name="TruncateProducts" value="MASS DELETE" style="padding-right: 30px;padding-left: 30px;padding-top: 10px;padding-bottom: 10px;"/>
					</form>
				</th>
				<th style="align-content: center;padding-right: 10px;">
					<form method="post">
						<input type="submit" name="AddDummyData" value="ADD Dummy Data" style="padding-right: 30px;padding-left: 30px;padding-top: 10px;padding-bottom: 10px;"/>						
					</form>
				</th>
			</tr>
		</table>
		<hr/>
	</header>
	<section style="width: 100%;display: flex;flex-wrap: wrap;margin-top: 15px;">
		<?php
		if($db->getResult() !== null && count($db->getResult()) !== 0){
			foreach ($db->getResult() as $key => $value) {
				$element = '<div style="border-color: #000000;width: 230px;min-height:160px;height: auto;border-radius: 10px;border-width: 3px;border-style: solid;margin-left: 10px;margin-bottom: 10px;padding-left:10px;padding-right:10px;">
				<form method="post"><input type="checkbox" id="ItemId" name="DELETE'.$key.'" value="'.$key.'" class="delete-checkbox"></form>
				<h3 style="text-align: center;margin:0;word-wrap: break-word;">'.$value->getSku().'</h3>
				<h3 style="text-align: center;margin:0;word-wrap: break-word;">'.$value->getName().'</h3>
				<h3 style="text-align: center;margin:0;word-wrap: break-word;">'.$value->getPrice().' $</h3>';
				if($value->getSize() !== null && !empty($value->getSize())){
					$element .= '<h3 style="text-align: center;margin:0;word-wrap: break-word;">Size: '.$value->getSize().' MB</h3>';
				}else if($value->getWeight() !== null && !empty($value->getWeight())){
					$element .= '<h3 style="text-align: center;margin:0;word-wrap: break-word;">Weight: '.$value->getWeight().' KG</h3>';
				}else{
					$element .= '<h3 style="text-align: center;margin:0;word-wrap: break-word;">Dimension: '.$value->getHeight().'<span style="color:red;">x</span>'.$value->getWidth().'<span style="color:red;">x</span>'.$value->getLength().' CM</h3>';
				}
				$element .= "</div>";
				echo $element;
			}
		}else{
			echo '<h1 style="text-align: center;width: 100%;height: 100%;">No Data To Show</h1>';
		}
		?>
	</section>
	<footer style="position: absolute;width: 100%;text-align: center;height: 100px;">
		<hr/>
		<h2>Scandiweb Test assignment</h2>
	</footer>
	<script type="text/javascript">
		var height = document.getElementsByTagName('section')[0].offsetHeight;//includes margin,border,padding
		if(height < 798){
			document.getElementsByTagName("footer")[0].style.bottom  = 0;
		}else{
			document.getElementsByTagName("footer")[0].style.bottom  = 1;
		}
		var checkbox = document.getElementsByClassName("delete-checkbox");
		for(var i = 0; i < checkbox.length; i++) {
			checkbox[i].addEventListener('change', function() {
				if (this.checked) {
					this.parentElement.submit();
				}
			});
		}

	</script>
</body>
</html>